Godot-Rust boilerplate demo for 64-bits Linux OS
===

This godot project is a starting point for making a godot-Rust game in 3D.

# What's included

- A screen title with a button to open the game
- A basic level with a minimal environment, sun and terrain blocks
- A 3rd person player with:
    - Back camera controlled by mouse
    - WASD movement and gravity
    - A basic Raycast which aims according the player's view
- A pause menu in `ui_cancel` button (ESC key)

The maing settings for godot project includes:
- Godot physics
- 60 FPS forced


# How does it work?

I'll add a tutorial later here.

# How to use it
- Clone this repo
- Setup Rust according your desired method (I prefer rustup)
- Open a terminal in the folder of the repo and use the following commands:
- `cargo clean`, `cargo build --release`
- Once build finishes, open the project in godot (root folder of the repo)
- Press "Play" and is ready :)

# Issues

Report me any issues with the code here using the tickets.

---

_Please if you want to use this code, take in mind GPLV3 license. As well, using this code/assets for make OC of any type MUST include a link to this repo, and mention to myself with my website (https://cristodcgomez.dev)_

Enjoy the learning :)