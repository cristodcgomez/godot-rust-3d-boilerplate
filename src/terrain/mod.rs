/*
  Example file for define a new Rust Object which will be instantiated in Godot.
  Take in mind that you need to match the Type of the node with the "ìnherit" declaration in line 10.
  Of course you need to set the library in the godot project in order to use this code.
*/
use gdnative::api::*;
use gdnative::prelude::*;

#[derive(Debug, NativeClass)]
#[inherit(StaticBody)]
pub struct Terrain {}

impl Terrain {
  fn new(_owner: &StaticBody) -> Self {
    Terrain {}
  }
}

#[methods]
impl Terrain {
  #[export]
  fn _ready(&mut self, _owner: &StaticBody) {}

  #[export]
  fn _process(&mut self, _owner: &StaticBody, _delta: f32) {}

  #[export]
  fn _on_timeout(&mut self, _owner: &StaticBody) {}
}
