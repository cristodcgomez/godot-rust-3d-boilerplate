use gdnative::prelude::*;

#[derive(Debug, NativeClass)]
#[inherit(Control)]
pub struct Pause;

impl Pause {
    fn new(_owner: &Control) -> Self {
        Pause
    }

    fn get_scene_tree(owner: &Control) -> TRef<SceneTree, Shared> {
        owner
            .get_tree()
            .map(|node| unsafe { node.assume_safe() })
            .and_then(|node| node.cast::<SceneTree>())
            .unwrap()
    }
}

#[methods]
impl Pause {
    #[export]
    fn _ready(&mut self, _owner: &Control) {}

    #[export]
    fn _process(&mut self, owner: &Control, _delta: f32) {
        let input = Input::godot_singleton();
        let tree = Pause::get_scene_tree(owner).as_ref();
        if Input::is_action_just_pressed(&input, "ui_cancel") {
            if tree.is_paused() {
                owner.hide();
                tree.set_pause(false);
            } else {
                owner.show();
                tree.set_pause(true);
            }
        }
    }
}
